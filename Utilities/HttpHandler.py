import requests
from .Exceptions import ServerError
from config import *

class HttpHandler():
    DELIMITER = '/'
    HTTP_PREFIX = "http://"

    @staticmethod
    def get(entity, path=""):
    
        response = requests.get(HttpHandler.HTTP_PREFIX +\
                IP_ADDRESS + ":" + str(PORT_NUMBER) +\
                HttpHandler.DELIMITER +\
                entity + HttpHandler.DELIMITER + path)

        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ServerError()

    @staticmethod
    def post(entity, data, path="/"):
    
        response = requests.post(HttpHandler.HTTP_PREFIX +\
                IP_ADDRESS + ":" + str(PORT_NUMBER) +\
                HttpHandler.DELIMITER +\
                entity + path, data=data)

        if response.status_code == requests.codes.ok:
            print("The pastebin URL is:%s"%response.text) 
        else:
            raise ServerError()
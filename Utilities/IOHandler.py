from tabulate import tabulate

class IOHandler():

    @staticmethod
    def print_json_as_table(json):
        if not type(json).__name__ == "list":
            json = [json]
        columns = json[0].keys()

        values = []
        for json_item in json:
            item_values = list(json_item.values())
            for i in range(len(item_values)):
                if type(item_values[i]).__name__ == "list" and type(item_values[i][0]).__name__ == "str":
                    item_values[i] = ', '.join(item_values[i])
            values.append(item_values)

        print(tabulate(values, headers=columns))
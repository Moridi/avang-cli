import click

from Graph import graph
from Dataset import dataset

@click.group()
def entry_point():
    pass

entry_point.add_command(graph)
entry_point.add_command(dataset)


if __name__ == '__main__':
    entry_point()
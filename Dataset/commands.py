import click

@click.group(help = "dataset commands")
def dataset():
    pass

@dataset.command()
def hello():
    """Display the current version."""
    click.echo("hello from dataset")

if __name__ == '__main__':
    dataset()
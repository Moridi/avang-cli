import click
import Utilities.HttpHandler as HttpHandler
import Utilities.IOHandler as IOHandler

@click.group(help = "graph commands", name="graph")
def graph():
    pass

@graph.command(help = "Get all the graphs", name="get")
@click.option('--limit', default=-1, help="Print all the graphs")
@click.option('--name', default=None, help="Get graph information")
def get(limit, name):
    if (name != None):
        IOHandler.print_json_as_table(HttpHandler.get("graphs", path=name))
    else:
        IOHandler.print_json_as_table(HttpHandler.get("graphs")[:limit])

@graph.command(help = "Update following graphs", name="update")
def update():
    pass;
    # temp = HttpHandler.get("graphs")
    # HttpHandler.post("graphs", data=temp)
